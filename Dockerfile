FROM alpine

RUN apk add --no-cache curl

RUN curl -fsSL https://github.com/miniflux/miniflux/releases/download/2.0.8/miniflux-linux-armv7 -o miniflux \
  && chmod +x miniflux


ENTRYPOINT [ "/miniflux" ]