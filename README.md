# miniflux-arm

miniflux container based on alpine-arm

## Usage

docker-compose.yml:

```yml
version: "3"

services:
  db:
    image: postgres:latest
    environment:
      - POSTGRES_PASSWORD=miniflux
      - POSTGRES_USER=miniflux
    volumes:
      - /swarm/volumes/services/miniflux/postgres:/var/lib/postgresql/data

  app:
    image: andybroger/miniflux-arm
    depends_on:
      - db
    environment:
      - DATABASE_URL=postgres://miniflux:miniflux@db/miniflux?sslmode=disable
      - RUN_MIGRATIONS=1
      # - CREATE_ADMIN=1 #uncomment on first run!
      # - ADMIN_USERNAME=admin #uncomment on first run!
      # - ADMIN_PASSWORD=test123 #uncomment on first run!
      - LISTEN_ADDR=0.0.0.0:8080
```
